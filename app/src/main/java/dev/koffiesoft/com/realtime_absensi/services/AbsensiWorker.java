package dev.koffiesoft.com.realtime_absensi.services;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import dev.koffiesoft.com.realtime_absensi.Constants;
import dev.koffiesoft.com.realtime_absensi.R;
import dev.koffiesoft.com.realtime_absensi.models.RealtimePostResponseModel;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static dev.koffiesoft.com.realtime_absensi.Constants.CHANNEL_ID;

public class AbsensiWorker extends Worker {

    private FusedLocationProviderClient mFusedLocation;

    public AbsensiWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        try {

//            mFusedLocation = LocationServices.getFusedLocationProviderClient(getApplicationContext());
//            mFusedLocation.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
//                @Override
//                public void onSuccess(Location location) {
//                    if (location != null) {
//                        double latitude = location.getLatitude();
//                        double longitude = location.getLongitude();
//
//                        String nip = Preferences.getNip(getApplicationContext());
//                        makeStatusNotification(nip + latitude + longitude);
//                    } else {
//                        String nip = Preferences.getNip(getApplicationContext());
//                        makeStatusNotification(nip);
//                    }
//                }
//            });

//            String nip = Preferences.getNip(getApplicationContext());
//            makeStatusNotification(nip);

            startLocationUpdate();
            return Result.success();
        } catch (Throwable t) {
            return  Result.failure();
        }
    }


    private void makeStatusNotification(String message) {

        Context context = getApplicationContext();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            CharSequence name = Constants.VERBOSE_NOTIFICATION_CHANNEL_NAME;
            String description = Constants.VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION;
            int importance = NotificationManager.IMPORTANCE_HIGH;

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationChannel.setDescription(description);

            NotificationManager notificationManager =(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        // Create the notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(Constants.NOTIFICATION_TITLE)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVibrate(new long[0]);


        // Show the notification
        NotificationManagerCompat.from(context).notify(Constants.NOTIFICATION_ID, builder.build());
    }

    // Make a request location updates
    @SuppressLint("MissingPermission")
    private void startLocationUpdate() {
        mFusedLocation = LocationServices.getFusedLocationProviderClient(getApplicationContext());


        mFusedLocation.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    double lat = location.getLatitude();
                    double longi = location.getLongitude();

                    String la = String.valueOf(lat);
                    String lo = String.valueOf(longi);
                    String loc = "Lokasi anda " + la + " " + lo;

                    String nip = Preferences.getNip(getApplicationContext());

                    Log.d("TAG LOCATION", "onSuccess: ");
                    Log.d("RESULT", la + lo);

//                    makeStatusNotification(loc);
                    sendToServer(nip, la, lo);
                } else {
                    Log.d("TAG LOCATION", "onSuccess failed: ");
                    makeStatusNotification("Gagal mendapatkan lokasi anda");
                }

            }
        });

    }


    private void sendToServer(String nip, String latitude, String longitude) {

        Retrofit retrofitApiClient = RetrofitApiClient.getRetrofitInstance();
        RealtimeAbsensiServices realtimeAbsensiServices = retrofitApiClient.create(RealtimeAbsensiServices.class);

        final RequestBody rNip = RequestBody.create(MediaType.parse("text/plain"), nip);
        final RequestBody rLatitude = RequestBody.create(MediaType.parse("text/plain"), latitude);
        final RequestBody rLongitude = RequestBody.create(MediaType.parse("text/plain"), longitude);

        Call<RealtimePostResponseModel> call = realtimeAbsensiServices.realtime(rNip, rLatitude, rLongitude);

        call.enqueue(new Callback<RealtimePostResponseModel>() {
            @Override
            public void onResponse(Call<RealtimePostResponseModel> call, Response<RealtimePostResponseModel> response) {
                if (response.code() == 200) {
                    makeStatusNotification("Realtime: Lokasi berhasil dikirim ke server");
                } else {
                    makeStatusNotification("Realtime: Lokasi gagal dikirim ke server");
                }
            }

            @Override
            public void onFailure(Call<RealtimePostResponseModel> call, Throwable t) {
                makeStatusNotification(t.getMessage());
            }
        });
    }



}
