package dev.koffiesoft.com.realtime_absensi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import dev.koffiesoft.com.realtime_absensi.models.UserLoginResponseModel;
import dev.koffiesoft.com.realtime_absensi.services.MD5;
import dev.koffiesoft.com.realtime_absensi.services.Preferences;
import dev.koffiesoft.com.realtime_absensi.services.RealtimeAbsensiServices;
import dev.koffiesoft.com.realtime_absensi.services.RetrofitApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private EditText mEditTextNIP, mEditTextPassword;
    private Button mButtonLogin;
    private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditTextNIP = (EditText) findViewById(R.id.editTextNIP);
        mEditTextPassword = (EditText) findViewById(R.id.editTextPassword);
        mButtonLogin = (Button) findViewById(R.id.buttonLogin);

        retrofit = RetrofitApiClient.getRetrofitInstance();

        // WHEN LOGIN BUTTON IS PRESSED
        mButtonLogin.setOnClickListener(loginListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        Preferences.setNip(getApplicationContext(), mEditTextNIP.getText().toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean exists = Preferences.checkNama(getApplicationContext());
        if (exists) {
            Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    private View.OnClickListener loginListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String username = mEditTextNIP.getText().toString();
            String password = mEditTextPassword.getText().toString();

            try {
                String hashPassword = MD5.getMd5(password);
                Toast.makeText(MainActivity.this, hashPassword, Toast.LENGTH_SHORT).show();

                login(username, hashPassword);

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "hashPassword failed", Toast.LENGTH_SHORT).show();

            }
        }
    };

    public void login(String username, String hashedPassword) {
        RealtimeAbsensiServices realtimeAbsensiServices = retrofit.create(RealtimeAbsensiServices.class);
        Call<List<UserLoginResponseModel>> call = realtimeAbsensiServices.userLogin(username, hashedPassword);

        call.enqueue(new Callback<List<UserLoginResponseModel>>() {
            @Override
            public void onResponse(Call<List<UserLoginResponseModel>> call, Response<List<UserLoginResponseModel>> response) {
                if (response.code() != 200) {
                    String errCode = String.valueOf(response.code());
                    Toast.makeText(MainActivity.this, errCode, Toast.LENGTH_SHORT).show();
                }


                String name = response.body().get(0).getNama();

                Toast.makeText(MainActivity.this, "Login success " + name, Toast.LENGTH_SHORT).show();

                String nip = response.body().get(0).getNip();
                String pangkat = response.body().get(0).getPangkat();

                Preferences.setNama(getApplicationContext(), name);
                Preferences.setNip(getApplicationContext(), nip);
                Preferences.setPangkat(getApplicationContext(), pangkat);

                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(Call<List<UserLoginResponseModel>> call, Throwable t) {
                String err = t.getMessage();
                Toast.makeText(MainActivity.this, err, Toast.LENGTH_SHORT).show();
            }
        });
    }

}