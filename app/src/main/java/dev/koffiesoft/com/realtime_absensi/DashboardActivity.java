package dev.koffiesoft.com.realtime_absensi;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.work.Constraints;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import dev.koffiesoft.com.realtime_absensi.models.LocationRangeResponseModel;
import dev.koffiesoft.com.realtime_absensi.models.UserAbsensiResponseModel;
import dev.koffiesoft.com.realtime_absensi.services.AbsensiWorker;
import dev.koffiesoft.com.realtime_absensi.services.Preferences;
import dev.koffiesoft.com.realtime_absensi.services.RealtimeAbsensiServices;
import dev.koffiesoft.com.realtime_absensi.services.RetrofitApiClient;
import dev.koffiesoft.com.realtime_absensi.services.TimeConstraints;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DashboardActivity extends AppCompatActivity {

    private TextView mTextViewInfoUser, mTextViewCurrentTime;
    private String name, nip, pangkat, latitude, longitude;
    private Button mButtonSignOut, mButtonGetLocation, mButtonAbsensi;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Retrofit retrofit;
    private boolean canAbsen;
    private WorkManager mWorkManager;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        canAbsen = false;
        mButtonSignOut = (Button) findViewById(R.id.buttonSignOut);
        mButtonGetLocation = (Button) findViewById(R.id.buttonGetLocation);
        mButtonAbsensi = (Button) findViewById(R.id.buttonAbsensi);

        mTextViewInfoUser = (TextView) findViewById(R.id.textViewInformasiUser);
        mTextViewCurrentTime = (TextView) findViewById(R.id.textViewCurrentTime);

        spinner = (ProgressBar) findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        retrofit = RetrofitApiClient.getRetrofitInstance();

        name = Preferences.getNama(getApplicationContext());
        nip = Preferences.getNip(getApplicationContext());
        pangkat = Preferences.getPangkat(getApplicationContext());

        mTextViewInfoUser.setText(name);
        mTextViewCurrentTime.append(getTime());

        // WHEN BUTTON ABSENSI PRESSED
        mButtonAbsensi.setOnClickListener(buttonAbsenListener);

        // WHEN BUTTON SIGN OUT PRESSED
        mButtonSignOut.setOnClickListener(buttonSignOutListener);

        // GO TO Location activity
        mButtonGetLocation.setOnClickListener(buttonGetLocationListener);

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        mWorkManager = WorkManager.getInstance(getApplicationContext());

        // prosesAbsen();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    private View.OnClickListener buttonAbsenListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            spinner.setVisibility(View.VISIBLE);
            prosesAbsen();
        }
    };

    private void prosesAbsen() {
        // 1. check waktu absen
        // 2. check akses lokasi saat ini
        // 3. check location range dari api
        // 4. absensi

        if (TimeConstraints.checkConstraintsCheckIn() || TimeConstraints.checkContraintsCheckOut()) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions();
                spinner.setVisibility(View.GONE);
                return;
            }
            mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    double lat = location.getLatitude();
                    double lon = location.getLongitude();

                    Toast.makeText(DashboardActivity.this, "Lokasi anda telah dikonfirmasi", Toast.LENGTH_SHORT).show();
//                    checkLocationRange(lat, lon);
                    checkLocationRange(-8.1171776, 111.7972506);
                }
            });
        } else {
            Toast.makeText(DashboardActivity.this, "Absensi gagal. Diluar waktu absensi", Toast.LENGTH_SHORT).show();
            spinner.setVisibility(View.GONE);
        }
    }

    private void checkLocationRange(double lat, double lon) {
        RealtimeAbsensiServices realtimeAbsensiServices = retrofit.create(RealtimeAbsensiServices.class);
        Call<List<LocationRangeResponseModel>> call = realtimeAbsensiServices.getLocationRange();

        call.enqueue(new Callback<List<LocationRangeResponseModel>>() {
            @Override
            public void onResponse(Call<List<LocationRangeResponseModel>> call, Response<List<LocationRangeResponseModel>> response) {
                if (response.code() == 200) {
                    Toast.makeText(DashboardActivity.this, "Mendapatkan data koordinat dari server", Toast.LENGTH_SHORT).show();

                    double latMin = response.body().get(0).getLatitudeMin();
                    double latMax = response.body().get(0).getLatitudeMax();

                    double longMin = response.body().get(0).getLongitudeMin();
                    double longMax = response.body().get(0).getLongitudeMax();

                    boolean latInRange = lat >= latMin && lat <= latMax;
                    boolean longInRange = lon >= longMin && lon <= longMax;

                    if (latInRange && longInRange) {
                        absensi(lat, lon);
                    } else {
                        Toast.makeText(DashboardActivity.this, "Absensi gagal. Di luar area absensi", Toast.LENGTH_SHORT).show();
                        spinner.setVisibility(View.GONE);
                    }

                } else {
                    Toast.makeText(DashboardActivity.this, "Absensi gagal. gagal mendapatkan koordinat dari server ", Toast.LENGTH_SHORT).show();
                    spinner.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<List<LocationRangeResponseModel>> call, Throwable t) {
                Toast.makeText(DashboardActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("DEBUG LOC RANGE", t.getMessage());
            }
        });
    }

    private OnSuccessListener<Location> onSuccessListenerLocation = new OnSuccessListener<Location>() {
        @Override
        public void onSuccess(Location location) {
            if (location != null) {
                double lat = location.getLatitude();
                double lon = location.getLongitude();

                latitude = String.valueOf(lat);
                longitude = String.valueOf(lon);
                Toast.makeText(DashboardActivity.this, "LAT LANG CONFIRMED", Toast.LENGTH_SHORT).show();
                absensi(lat, lon);
            } else Toast.makeText(DashboardActivity.this, "NULL LOC", Toast.LENGTH_SHORT).show();
        }
    };

    private View.OnClickListener buttonGetLocationListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (ActivityCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions();
                startLocationUpdate();
                return;
            }

            startLocationUpdate();
            // mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(onSuccessListenerLocation);
            // Toast.makeText(DashboardActivity.this, "PERMISSION GRANTED", Toast.LENGTH_SHORT).show();
        }
    };


    private View.OnClickListener buttonSignOutListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//            Preferences.clearPrefs(getApplicationContext());
//            Intent intent = new Intent(DashboardActivity.this, MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            finish();

            setPeriodicRequests();
        }
    };

    private String getTime() {
        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

        return currentTime + "\n" + currentDate;
    }

    private void absensi(double latitudes, double longitudes) {

        final RequestBody rNama = RequestBody.create(MediaType.parse("text/plain"), name);
        final RequestBody rNip = RequestBody.create(MediaType.parse("text/plain"), nip);
        final RequestBody rPangkat = RequestBody.create(MediaType.parse("text/plain"), pangkat);
        final RequestBody rLatitude = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(latitudes));
        final RequestBody rLongitude = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(longitudes));

        RealtimeAbsensiServices realtimeAbsensiServices = retrofit.create(RealtimeAbsensiServices.class);
        Call<UserAbsensiResponseModel> call = realtimeAbsensiServices.userAbsensi(rNama, rPangkat, rNip, rLatitude, rLongitude);

        call.enqueue(new Callback<UserAbsensiResponseModel>() {
            @Override
            public void onResponse(Call<UserAbsensiResponseModel> call, Response<UserAbsensiResponseModel> response) {
                if (response.code() == 200) {
                    Toast.makeText(DashboardActivity.this, "ABSENSI BERHASIL", Toast.LENGTH_SHORT).show();
                    spinner.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<UserAbsensiResponseModel> call, Throwable t) {
                Log.d("DEBUG", t.getMessage());
            }
        });
    }

    private void requestPermissions() {
        // REQUEST FOR PERMISSIONS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            Toast.makeText(DashboardActivity.this, "MAKE PERMISSIONS", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
            }, 44);
        }
    }

    private void setPeriodicRequests() {
        PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(AbsensiWorker.class, 15, TimeUnit.MINUTES, 5, TimeUnit.MINUTES)
                .setConstraints(Constraints.NONE)
                .build();

        OneTimeWorkRequest oneTimeWorkRequest = new OneTimeWorkRequest.Builder(AbsensiWorker.class).build();

        mWorkManager.enqueue(periodicWorkRequest);

    }


    @SuppressLint("MissingPermission")
    private void startLocationUpdate() {
        LocationRequest locationRequest = new LocationRequest();

        mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);

            if (locationResult != null) {
                double longitude = locationResult.getLocations().get(0).getLongitude();
                double latitude = locationResult.getLocations().get(0).getLatitude();


                Toast.makeText(DashboardActivity.this, String.valueOf(latitude), Toast.LENGTH_SHORT).show();
            } else Toast.makeText(DashboardActivity.this, "NULL ", Toast.LENGTH_SHORT).show();

        }
    };
}