package dev.koffiesoft.com.realtime_absensi.services;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class TimeConstraints {

    public static boolean checkConstraintsCheckIn() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            LocalDateTime timeNow = LocalDateTime.now();

            String currentDate = getCurrentDate();
            String startAbsenMasuk = currentDate + " " + "08:00";
            String endAbsenMasuk = currentDate + " " + "09:00";

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            LocalDateTime ltStartAbsenMasuk = LocalDateTime.parse(startAbsenMasuk, formatter);
            LocalDateTime ltEndAbsenMasuk = LocalDateTime.parse(endAbsenMasuk, formatter);

            return timeNow.isAfter(ltStartAbsenMasuk) && timeNow.isBefore(ltEndAbsenMasuk);

        } else return false;
    };

    public static boolean checkContraintsCheckOut() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            LocalDateTime timeNow = LocalDateTime.now();

            String currentDate = getCurrentDate();
            String startAbsenPulang = currentDate + " " + "16:00";
            String endAbsenPulang = currentDate + " " + "21:00";

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            LocalDateTime ltStartAbsenPulang = LocalDateTime.parse(startAbsenPulang, formatter);
            LocalDateTime ltEndAbsenPulang = LocalDateTime.parse(endAbsenPulang, formatter);

            return timeNow.isAfter(ltStartAbsenPulang) && timeNow.isBefore(ltEndAbsenPulang);
        } else return false;
    };

    private static String getCurrentDate() {
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        return currentDate;
    }
}
