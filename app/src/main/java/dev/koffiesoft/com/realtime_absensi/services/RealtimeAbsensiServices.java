package dev.koffiesoft.com.realtime_absensi.services;

import java.util.List;

import dev.koffiesoft.com.realtime_absensi.models.LocationRangeResponseModel;
import dev.koffiesoft.com.realtime_absensi.models.RealtimePostResponseModel;
import dev.koffiesoft.com.realtime_absensi.models.UserAbsensiResponseModel;
import dev.koffiesoft.com.realtime_absensi.models.UserLoginResponseModel;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RealtimeAbsensiServices {

    // this file will be place to all of our API endpoints
    // for login user
    @GET("login")
    Call<List<UserLoginResponseModel>> userLogin(@Query("nip") String nip, @Query("password") String password);

    @Multipart
    @POST("absensi")
    Call<UserAbsensiResponseModel> userAbsensi(@Part("nama") RequestBody rNama,
                                               @Part("pangkat") RequestBody rPangkat,
                                               @Part("nip") RequestBody rNip,
                                               @Part("latitude") RequestBody rLatitude,
                                               @Part("longitude") RequestBody rLongitude);

    @GET("LocationRange")
    Call<List<LocationRangeResponseModel>> getLocationRange();

    @Multipart
    @POST("realtime")
    Call<RealtimePostResponseModel> realtime(@Part("nip") RequestBody rNip,
                                             @Part("longitude") RequestBody rLongitude,
                                             @Part("latitude") RequestBody rLatitude);

}
